import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

	constructor(
		private authService: AuthService,
		private router: Router
	){}

	//"canActivate" funciona independientemente si se cargó el módulo simpre que no se cumpla la condición el usuario
	//no podrá acceder a la ruta aunque el módulo haya sido cargado
	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
			
			return this.authService.verificaAutenticacion()
				.pipe(
					tap(estaAutenticado => {
						if (!estaAutenticado) {
							this.router.navigate(['./auth/login']);
						}
					})
				);

			// if (this.authService.auth.id) {
			// 	return true;
			// }
			// console.log('Bloqueado por canActivate');
			// return false;
		}
	
	//"canLoad" solo restringe que se pueda cargar el módulo
	//si el módulo ya fue cargado entonces este método no funcionará y dejará ir a la ruta
	//Esto se utiliza solo si usamos "Lazyload" en caso contrario solo debemos usar canActivate
	canLoad(
		route: Route,
		segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
			
			return this.authService.verificaAutenticacion()
				.pipe(
					tap(estaAutenticado => {
						if (!estaAutenticado) {
							this.router.navigate(['./auth/login']);
						}
					})
				);
			
			// if (this.authService.auth.id) {
			// 	return true;
			// }
			// console.log('Bloqueado por canLoad');
			// return false;
		}
}
